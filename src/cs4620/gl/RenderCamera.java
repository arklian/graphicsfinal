package cs4620.gl;

import cs4620.common.SceneCamera;
import cs4620.common.SceneObject;
import egl.math.Matrix4;
import egl.math.Vector2;
import egl.math.Vector2d;

public class RenderCamera extends RenderObject {
	/**
	 * Reference to Scene counterpart of this camera
	 */
	public final SceneCamera sceneCamera;
	
	/**
	 * The view transformation matrix
	 */
	public final Matrix4 mView = new Matrix4();
	
	/**
	 * The projection matrix
	 */
	public final Matrix4 mProj = new Matrix4();
	
	/**
	 * The viewing/projection matrix (The product of the view and projection matrices)
	 */
	public final Matrix4 mViewProjection = new Matrix4();
	
	/**
	 * The size of the viewport, in pixels.
	 */
	public final Vector2 viewportSize = new Vector2();
	
	public RenderCamera(SceneObject o, Vector2 viewSize) {
		super(o);
		sceneCamera = (SceneCamera)o;
		viewportSize.set(viewSize);
	}

	/**
	 * Update the camera's viewing/projection matrix.
	 * 
	 * Update the camera's viewing/projection matrix in response to any changes in the camera's transformation
	 * or viewing parameters.  The viewing and projection matrices are computed separately and multiplied to 
	 * form the combined viewing/projection matrix.  When computing the projection matrix, the size of the view
	 * is adjusted to match the aspect ratio (the ratio of width to height) of the viewport, so that objects do 
	 * not appear distorted.  This is done by increasing either the height or the width of the camera's view,
	 * so that more of the scene is visible than with the original size, rather than less.
	 *  
	 * @param viewportSize
	 */
	public void updateCameraMatrix(Vector2 viewportSize) {
		this.viewportSize.set(viewportSize);
		
		// The camera's transformation matrix is found in this.mWorldTransform (inherited from RenderObject).
		// The other camera parameters are found in the scene camera (this.sceneCamera).
		// Look through the methods in Matrix4 before you type in any matrices from the book or the OpenGL specification.
		
		// TODO#A3 SOLUTION START
		
		// Create viewing matrix
		mView.set(this.mWorldTransform).invert();
		
		// Correct Image Aspect Ratio By Enlarging Image
		double a = this.sceneCamera.imageSize.x / viewportSize.x ;
		double b = this.sceneCamera.imageSize.y / viewportSize.y ;
		if(this.sceneCamera.isPerspective){
			if(a>b){this.sceneCamera.setPerspective(new Vector2d(this.sceneCamera.imageSize.x,viewportSize.y * a ));}
			else{this.sceneCamera.setPerspective(new Vector2d(viewportSize.x * b, this.sceneCamera.imageSize.y ));}
		}
		else{
			if(a>b){this.sceneCamera.setOrthographic(new Vector2d(this.sceneCamera.imageSize.x,viewportSize.y * a ));}
			else{this.sceneCamera.setOrthographic(new Vector2d(viewportSize.x * b, this.sceneCamera.imageSize.y ));}
		}	
		
		// Create Projection

		float width = (float)this.sceneCamera.imageSize.x;
		float height = (float)this.sceneCamera.imageSize.y;
		float near = (float)this.sceneCamera.zPlanes.x;
		float far = (float)this.sceneCamera.zPlanes.y;
		
		if(this.sceneCamera.isPerspective){mProj.set(Matrix4.createPerspective(width, height, near, far));}
		else{mProj.set(Matrix4.createOrthographic(width, height, near, far));}
		
		// Set the view projection matrix using the view and projection matrices
		mViewProjection.set(mView.clone().mulAfter(mProj));
		// SOLUTION END
	}	
}