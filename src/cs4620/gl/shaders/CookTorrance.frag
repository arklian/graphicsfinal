#version 120

// You May Use The Following Functions As RenderMaterial Input
// vec4 getDiffuseColor(vec2 uv)
// vec4 getNormalColor(vec2 uv)
// vec4 getSpecularColor(vec2 uv)

// Lighting Information
const int MAX_LIGHTS = 16;
uniform int numLights;
uniform vec3 lightIntensity[MAX_LIGHTS];
uniform vec3 lightPosition[MAX_LIGHTS];
uniform vec3 ambientLightIntensity;
// Camera Information
uniform mat4 mViewProjection;
uniform vec3 worldCam;
uniform float exposure;


// Shading Information
// 0 : smooth, 1: rough
uniform mat4 mWorld;
uniform mat3 mWorldIT;
uniform float shininess;
uniform float roughness;


varying vec2 fUV;
varying vec3 fN; // normal at the vertex
varying vec4 worldPos; // vertex position in world coordinates



void main() {
  // TODO A4
  vec3 N = normalize(fN);
  vec3 V = normalize(worldCam - worldPos.xyz);
  vec4 finalColor = vec4(0.0, 0.0, 0.0, 0.0);
  for(int i = 0; i < numLights; i++){
  	float r = length(lightPosition[i] - worldPos.xyz);
	vec3 L = normalize(lightPosition[i] - worldPos.xyz); 
	vec3 H = normalize(L + V);
	
  	float fresnel = .04 + ((1 - .04) * pow(1 - dot(V,H),5));
  	float d = 1/(roughness * roughness * pow(dot(N,H),4)) * exp((pow(dot(N,H),2)-1)/(roughness * roughness * pow(dot(N,H),2)));
  	float g = min(1,2*dot(N,H)*dot(N,V)/dot(V,H));
  	g = min(g,2*dot(N,H)*dot(N,L)/dot(V,H));
  	vec4 ks = getSpecularColor(fUV)*(fresnel / 3.14159265358979323) * (d * g)/(dot(N, V)*dot(N,L)) * max(dot(N,L),0.0);
  	vec4 kd = getDiffuseColor(fUV)* max(dot(N,L),0.0);
  	finalColor += vec4(lightIntensity[i], 0.0) * (kd + ks) / (r*r);
  	
  	
  }
  vec4 Iamb = getDiffuseColor(fUV);
  gl_FragColor =  (finalColor + vec4(ambientLightIntensity, 0.0)* Iamb) * exposure ; 	
}