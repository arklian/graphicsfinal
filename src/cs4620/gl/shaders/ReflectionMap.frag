#version 120

// You May Use The Following Functions As RenderMaterial Input
// vec4 getDiffuseColor(vec2 uv)
// vec4 getNormalColor(vec2 uv)
// vec4 getSpecularColor(vec2 uv)

// Lighting Information
// Camera Information
uniform vec3 worldCam;


// Shading Information
// 0 : smooth, 1: rough


varying vec2 fUV;
varying vec3 fN; // normal at the vertex
varying vec4 worldPos; // vertex position in world coordinates



void main() {
  // TODO A4
  vec3 N = normalize(fN);
  vec3 V = normalize(worldCam - worldPos.xyz);
  vec3 W = normalize((2 * (dot(V,N)*N)) - V);
  gl_FragColor = getEnvironmentColor(W);
}