package cs4620.ray2.shader;

import cs4620.ray2.RayTracer;
import cs4620.ray2.IntersectionRecord;
import cs4620.ray2.Ray;
import cs4620.ray2.Scene;
import egl.math.Colord;
import egl.math.Vector3d;

/**
 * A Phong material.
 *
 * @author ags, pramook
 */
public class Glass extends Shader {

	/**
	 * The index of refraction of this material. Used when calculating Snell's Law.
	 */
	protected double refractiveIndex;
	public void setRefractiveIndex(double refractiveIndex) { this.refractiveIndex = refractiveIndex; }


	public Glass() { 
		refractiveIndex = 1.0;
	}

	/**
	 * @see Object#toString()
	 */
	public String toString() {    
		return "glass " + refractiveIndex + " end";
	}

	/**
	 * Evaluate the intensity for a given intersection using the Glass shading model.
	 *
	 * @param outIntensity The color returned towards the source of the incoming ray.
	 * @param scene The scene in which the surface exists.
	 * @param ray The ray which intersected the surface.
	 * @param record The intersection record of where the ray intersected the surface.
	 * @param depth The recursion depth.
	 */
	@Override
	public void shade(Colord outIntensity, Scene scene, Ray ray, IntersectionRecord record, int depth) {
		// TODO#A7: fill in this function.
        // 1) Determine whether the ray is coming from the inside of the surface or the outside.
        // 2) Determine whether total internal reflection occurs.
        // 3) Compute the reflected ray and refracted ray (if total internal reflection does not occur)
        //    using Snell's law and call RayTracer.shadeRay on them to shade them
		outIntensity.setZero();
		Vector3d outgoing = ray.origin.clone().sub(record.location).normalize();
		Vector3d norm = record.normal.clone().normalize();
		Vector3d negNorm = norm.clone().negate();
		Vector3d s1 = new Vector3d();
		Vector3d s2 = new Vector3d();
		Vector3d reflected = new Vector3d();
		Vector3d refracted = new Vector3d();
		boolean total = false;
		double dotProd = outgoing.dot(norm);
		if (dotProd > 0){ //outside
			s1.set(outgoing.clone().sub(norm.clone().mul(outgoing.dot(norm))));
			s2.set(s1.clone().negate().mul(1.0/refractiveIndex));
			reflected.set(norm.clone().mul(2*norm.dot(outgoing)).sub(outgoing)).normalize();
			total = s2.len() > 1;
			if (!total){
				refracted.set(s2.clone().add(negNorm.clone().mul(Math.sqrt(1-s2.lenSq()))));
			}
		}
		else { //inside
			s1.set(outgoing.clone().sub(negNorm.clone().mul(outgoing.dot(negNorm))));
			s2.set(s1.clone().negate().mul(refractiveIndex));
			reflected.set(negNorm.clone().mul(2*negNorm.dot(outgoing)).sub(outgoing)).normalize();
			total = s2.len() > 1;
			if (!total){
				refracted.set(s2.clone().add(norm.clone().mul(Math.sqrt(1-s2.lenSq()))));
			}
		}
		Ray reflectedRay = new Ray(record.location, reflected);
		reflectedRay.start = ray.start;
		reflectedRay.end = Integer.MAX_VALUE;
		if (total){
			RayTracer.shadeRay(outIntensity, scene, reflectedRay, depth+1);
		}
		else{
			Ray refractedRay = new Ray(record.location, refracted); 
			refractedRay.start = ray.start;
			refractedRay.end = Integer.MAX_VALUE;
			double r = fresnel(norm, outgoing, refractiveIndex);
			Colord c1 = new Colord();
			Colord c2 = new Colord();
			RayTracer.shadeRay(c1, scene, reflectedRay, depth+1);
			RayTracer.shadeRay(c2, scene, refractedRay, depth+1);
			outIntensity.set(c1.mul(r).add(c2.mul(1-r)));
		}
	}

}