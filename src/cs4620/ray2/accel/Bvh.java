
package cs4620.ray2.accel;

import java.util.Arrays;
import java.util.Comparator;

import cs4620.ray2.IntersectionRecord;
import cs4620.ray2.Ray;
import cs4620.ray2.surface.Surface;
import egl.math.Vector3d;

/**
 * Class for Axis-Aligned-Bounding-Box to speed up the intersection look up time.
 *
 * @author ss932, pramook
 */
public class Bvh implements AccelStruct {   
	/** A shared surfaces array that will be used across every node in the tree. */
	private Surface[] surfaces;

	/** A comparator class that can sort surfaces by x, y, or z coordinate.
	 *  See the subclass declaration below for details.
	 */
	static MyComparator cmp = new MyComparator();
	
	/** The root of the BVH tree. */
	BvhNode root;

	public Bvh() { }

	/**
	 * Set outRecord to the first intersection of ray with the scene. Return true
	 * if there was an intersection and false otherwise. If no intersection was
	 * found outRecord is unchanged.
	 *
	 * @param outRecord the output IntersectionRecord
	 * @param ray the ray to intersect
	 * @param anyIntersection if true, will immediately return when found an intersection
	 * @return true if and intersection is found.
	 */
	public boolean intersect(IntersectionRecord outRecord, Ray rayIn, boolean anyIntersection) {
		
		 boolean ans = intersectHelper(root, outRecord, rayIn, anyIntersection);
		 //System.out.println("OneIntersect");
		 return ans;
	}
	
	/**
	 * A helper method to the main intersect method. It finds the intersection with
	 * any of the surfaces under the given BVH node.  
	 *   
	 * @param node a BVH node that we would like to find an intersection with surfaces under it
	 * @param outRecord the output InsersectionMethod
	 * @param rayIn the ray to intersect
	 * @param anyIntersection if true, will immediately return when found an intersection
	 * @return true if an intersection is found with any surface under the given node
	 */
	private boolean intersectHelper(BvhNode node, IntersectionRecord outRecord, Ray rayIn, boolean anyIntersection)
	{
		// TODO#A7: fill in this function.
		// Hint: For a leaf node, use a normal linear search. Otherwise, search in the left and right children.
		// Another hint: save time by checking if the ray intersects the node first before checking the childrens.
		IntersectionRecord temp = new IntersectionRecord();
		boolean intersected = false;
		if (node.intersects(rayIn)){
			//intersected
			if(node.isLeaf()){
				//System.out.println("isLeaf");
				outRecord.t = Double.MAX_VALUE;
				for(int i = node.surfaceIndexStart; i < node.surfaceIndexEnd; i++){
					if(surfaces[i].intersect(temp, rayIn)){
						if(temp.t < outRecord.t){

							outRecord.set(temp);
							intersected = true;
							if (anyIntersection){
								return true;
							}
						}
					}
					 
				}
				
				return intersected;
				
			}
			else{
				if(anyIntersection){
					return intersectHelper(node.child[0],outRecord,rayIn,anyIntersection) || intersectHelper(node.child[1],outRecord,rayIn,anyIntersection);
					
				}
				IntersectionRecord temp1 = new IntersectionRecord();
				IntersectionRecord temp2 = new IntersectionRecord();
				temp1.t = Double.MAX_VALUE;
				temp2.t = Double.MAX_VALUE;
				boolean left = intersectHelper(node.child[0],temp1,rayIn,anyIntersection);
				boolean right = intersectHelper(node.child[1],temp2,rayIn,anyIntersection);
				
				if(left && temp1.t <= temp2.t){
					
					outRecord.set(temp1);
					
				}
				else if(right && temp2.t < temp1.t){
					
					
					outRecord.set(temp2);
					
				}

				return left || right;
			}
			
		}
		else{
			//System.out.println("skipped");
			return false;
		}
	}


	@Override
	public void build(Surface[] surfaces) {
		this.surfaces = surfaces;
		root = createTree(0, surfaces.length);
		//System.out.print(root);
	}	
	
	/**
	 * Create a BVH [sub]tree.  This tree node will be responsible for storing
	 * and processing surfaces[start] to surfaces[end-1]. If the range is small enough,
	 * this will create a leaf BvhNode. Otherwise, the surfaces will be sorted according
	 * to the axis of the axis-aligned bounding box that is widest, and split into 2
	 * children.
	 * 
	 * @param start The start index of surfaces
	 * @param end The end index of surfaces
	 */
	private BvhNode createTree(int start, int end) {
		// TODO#A7: fill in this function.
		//System.out.println(start +":"+ end);
		// ==== Step 1 ====
		// Find out the BIG bounding box enclosing all the surfaces in the range [start, end)
		// and store them in minB and maxB.
		// Hint: To find the bounding box for each surface, use getMinBound() and getMaxBound() */
		Vector3d max = new Vector3d(Double.NEGATIVE_INFINITY); 
		Vector3d min = new Vector3d(Double.POSITIVE_INFINITY); 
		for(int i = start; i < end; i++){
			max.x = Math.max(surfaces[i].getMaxBound().x,max.x);
			max.y = Math.max(surfaces[i].getMaxBound().y,max.y);
			max.z = Math.max(surfaces[i].getMaxBound().z,max.z);

			min.x = Math.min(surfaces[i].getMinBound().x,min.x);
			min.y = Math.min(surfaces[i].getMinBound().y,min.y);
			min.z = Math.min(surfaces[i].getMinBound().z,min.z);
        }
		
		
		// ==== Step 2 ====
		// Check for the base case. 
		// If the range [start, end) is small enough (e.g. less than or equal to 10), just return a new leaf node.
		if(end-start < 15){
			return new BvhNode(min,max,null,null,start,end);
		}

		// ==== Step 3 ====
		// Figure out the widest dimension (x or y or z).
		// If x is the widest, set widestDim = 0. If y, set widestDim = 1. If z, set widestDim = 2.
		double difx = max.x - min.x;
		double dify = max.y - min.y;
		double difz = max.z - min.z;
		int widestDim = 0;
		if(difx > dify){
			if(difx > difz){
				widestDim = 0;
			}
			else{
				widestDim = 2;
			}
		}
		else{
			if(dify > difz){
				widestDim = 1;
			}
			else{
				widestDim = 2;
			}
		}
		
		
		// ==== Step 4 ====
		// Sort surfaces according to the widest dimension.
		MyComparator c = new MyComparator();
		c.setIndex(widestDim);
		Arrays.sort(surfaces,start,end, c);

		// ==== Step 5 ====
		// Recursively create left and right children.
		int div = (start + end) / 2;
		
		BvhNode left = createTree(start, div);
		BvhNode right = createTree(div, end);
		
		return new BvhNode(min,max,left,right,start,end);
				
        
       
	}

}

/**
 * A subclass that compares the average position two surfaces by a given
 * axis. Use the setIndex(i) method to select which axis should be considered.
 * i=0 -> x-axis, i=1 -> y-axis, and i=2 -> z-axis.  
 *
 */
class MyComparator implements Comparator<Surface> {
	int index;
	public MyComparator() {  }

	public void setIndex(int index) {
		this.index = index;
	}

	public int compare(Surface o1, Surface o2) {
		double v1 = o1.getAveragePosition().get(index);
		double v2 = o2.getAveragePosition().get(index);
		if(v1 < v2) return 1;
		if(v1 > v2) return -1;
		return 0;
	}

}
