package cs4620.splines;
import java.util.ArrayList;

import egl.math.Matrix4;
import egl.math.Vector2;
import egl.math.Vector4;

public class BSpline extends SplineCurve{

	public BSpline(ArrayList<Vector2> controlPoints, boolean isClosed,
			float epsilon) throws IllegalArgumentException {
		super(controlPoints, isClosed, epsilon);
	}

	@Override
	public CubicBezier toBezier(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3,
			float eps) {
		
		float c = 1.0f/6.0f;
		Matrix4 mat = new Matrix4(
				c,4*c,c,0,
				0,4*c,2*c,0,
				0,2*c,4*c,0,
				0,c,4*c,c					
				);
		Vector4 x = new Vector4(p0.x,p1.x,p2.x,p3.x);
		Vector4 y = new Vector4(p0.y,p1.y,p2.y,p3.y);
		mat.mul(x);
		mat.mul(y);
		
		return new CubicBezier(new Vector2(x.x,y.x), new Vector2(x.y,y.y), new Vector2(x.z,y.z), new Vector2(x.w,y.w), eps);
		//END SOLUTION0
	}
	

		
	
}
